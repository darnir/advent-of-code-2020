use aoc_runner_derive::{aoc, aoc_generator};

#[derive(Debug)]
struct Input {
    min: usize,
    max: usize,
    letter: char,
    password: String,
}

#[aoc_generator(day2)]
fn parse_day2(input: &str) -> Vec<Input> {
    input
        .lines()
        .filter_map(|l| {
            let mut iter = l.split(['-', ' ', ':'].as_ref()).filter(|s| !s.is_empty());
            Some(Input {
                min: iter.next()?.parse().ok()?,
                max: iter.next()?.parse().ok()?,
                letter: iter.next()?.chars().next()?,
                password: iter.next()?.to_owned(),
            })
        })
        .collect()
}

/// Naive first attempt.
///
/// Nothing interesting here. Just compute the number of times the needed
/// character is found in the password and compare against the policy.
#[aoc(day2, part1, try1)]
fn first_try(inputs: &[Input]) -> usize {
    inputs
        .iter()
        .filter(|l| {
            let letter_count = l.password.chars().filter(|&c| c == l.letter).count();
            letter_count >= l.min && letter_count <= l.max
        })
        .count()
}

/// Quick first attempt at Part 2.
///
/// This is a quick and dirty attempt at solving Part 2.
/// I say dirty because this is actually unsafe code. It will panic if
/// l.max > password.len(). Direct indexing like this is not safe, but it
/// is indeed the fastest.
#[aoc(day2, part2, try1)]
fn part2_naive(inputs: &[Input]) -> usize {
    inputs
        .iter()
        .filter(|l| {
            let chars = l.password.as_bytes();
            (chars[l.min - 1] as char == l.letter) ^ (chars[l.max - 1] as char == l.letter)
        })
        .count()
}

/// Safe variant of the first attempt.
///
/// This is a "safe" variant of the first attempt. It checks if max is greater
/// than the password length and returns early if that is the case. This incurs
/// a very slight performance cost, practically imperceptible.
/// This method is still not fully correct or safe. It will only work for
/// ASCII input. UTF-8 input will completely break it.
#[aoc(day2, part2, early_return)]
fn part2_early_return(inputs: &[Input]) -> usize {
    inputs
        .iter()
        .filter(|l| {
            if l.password.len() < l.max {
                return false;
            }
            let chars = l.password.as_bytes();
            (chars[l.min - 1] as char == l.letter) ^ (chars[l.max - 1] as char == l.letter)
        })
        .count()
}

/// The really safe method.
///
/// This is the really safe method, by iterating over characters.
/// As a result, it checks for the nth character position which may
/// not be the nth byte in UTF-8 strings.
/// This unicode handling however, does impose a performance cost.
#[aoc(day2, part2, BetterIterators)]
fn part2_iter(inputs: &[Input]) -> usize {
    inputs
        .iter()
        .filter(|l| {
            let mut chars = l.password.chars();
            (chars.nth(l.min - 1) == Some(l.letter))
                ^ (chars.nth(l.max - l.min - 1) == Some(l.letter))
        })
        .count()
}

#[cfg(test)]
mod tests {

    use super::*;

    fn get_sample() -> Vec<Input> {
        vec![
            Input {
                min: 1,
                max: 3,
                letter: 'a',
                password: "abcde".to_string(),
            },
            Input {
                min: 1,
                max: 3,
                letter: 'b',
                password: "cdefg".to_string(),
            },
            Input {
                min: 2,
                max: 9,
                letter: 'c',
                password: "ccccccccc".to_string(),
            },
        ]
    }

    #[test]
    fn part1() {
        assert_eq!(first_try(&get_sample()), 2);
    }

    #[test]
    fn part2() {
        assert_eq!(part2_naive(&get_sample()), 1);
        assert_eq!(part2_iter(&get_sample()), 1);
        assert_eq!(part2_early_return(&get_sample()), 1);
    }
}
