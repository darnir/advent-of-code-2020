use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day6)]
fn parse(input: &str) -> Vec<Vec<u32>> {
    input
        .split("\n\n")
        .map(|inp| {
            inp.lines()
                .map(|l| l.as_bytes().iter().map(|c| 1u32 << (c - b'a')).sum())
                .collect()
        })
        .collect::<Vec<_>>()
}

#[aoc(day6, part1)]
fn part1(input: &[Vec<u32>]) -> u32 {
    input
        .iter()
        .map(|g| g.iter().fold(0, |acc, p| acc | p).count_ones())
        .sum()
}

#[aoc(day6, part2)]
fn part2(input: &[Vec<u32>]) -> u32 {
    input
        .iter()
        .map(|g| g.iter().fold(u32::MAX, |acc, p| acc & p).count_ones())
        .sum()
}

#[test]
fn test_part() {
    let INPUT = "abc

a
b
c

ab
ac

a
a
a
a

b";
    assert_eq!(part1(&parse(INPUT)), 11);
}
