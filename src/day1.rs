use std::collections::HashMap;

use anyhow::{anyhow, Result};
use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day1)]
fn parse_input_day1(input: &str) -> Result<Vec<u32>> {
    let mut a = input
        .lines()
        .map(|l| l.parse())
        .collect::<Result<Vec<u32>, _>>()?;
    a.sort_unstable();
    Ok(a)
}

/// The naive O(n^2) solution to the problem.
///
/// Takes a number and looks through the list to find the complementing number.
/// Nothing fancy in this implementation
#[aoc(day1, part1, try1)]
fn part1(accounts: &[u32]) -> u32 {
    if let Some((num1, num2)) = find_sum_two(accounts, 2020) {
        num1 * num2
    } else {
        0
    }
}

/// Same as part1(), but it returns a Result.
///
/// This is the more idiomatic solution of the two. I implemented to see under
/// benchmarks exactly how much more expensive it would be. This variant has
/// a very slight cost penalty attached to it. But it is also the more correct
/// solution since it responds with an Err rather than 0.
#[aoc(day1, part1, ReturnResult)]
fn part1_result(accounts: &[u32]) -> Result<u32> {
    find_sum_two(accounts, 2020)
        .map(|(n1, n2)| n1 * n2)
        .ok_or_else(|| anyhow!("Invalid Input!"))
}

/// Use a HashMap to track the known combinations.
///
/// This was an intriguing idea for a potentially faster solution.
/// It tracks every number and its complement in a HashMap until it finds
/// that combination again. While this might be faster in theory, it is
/// significantly slower under a benchmark. My conjecture is that needing
/// to touch the memory at runtime induces a very significant cost which
/// outweighs any algorithmic gains.
#[aoc(day1, part1, HashMap)]
fn part1_hashmap(accounts: &[u32]) -> u32 {
    let mut lookup = HashMap::new();
    for i in accounts {
        if let Some(n2) = lookup.get(i) {
            return i * n2;
        } else {
            lookup.insert(2020 - i, *i);
        }
    }
    0
}

#[aoc(day1, part2, try1)]
fn part2(accounts: &[u32]) -> u32 {
    for i in accounts.iter() {
        if let Some((num1, num2)) = find_sum_two(accounts, 2020 - i) {
            return num1 * num2 * i;
        }
    }
    0
}

/// Use a narrowing search to find the solution.
///
/// The original attempt at Part 2 was made in a lazy fashion with me trying
/// to just reuse the code from Part 1. However, when a discussing with a
/// friend, I was shown a better way to do this. So I implemented a direct
/// translation of that friend's C++ code into Rust. It runs atleast 20x faster
/// than the old implementation and I'm not surprised. This is algorithmically
/// significantly superior to the other implementation.
#[aoc(day1, part2, Search)]
fn part2_search(accounts: &[u32]) -> u32 {
    for (index, i) in accounts.iter().enumerate() {
        let mut mid = (index + 1) as usize;
        let mut end = accounts.len() - 1;
        while mid < end {
            let rem = 2020 - (i + accounts[mid] + accounts[end]) as i32;
            if rem == 0 {
                return i * accounts[mid] * accounts[end];
            } else if rem > 0 {
                mid += 1;
            } else {
                end -= 1;
            }
        }
    }
    unreachable!();
}

fn find_sum_two(numbers: &[u32], target: u32) -> Option<(u32, u32)> {
    for i in numbers.iter() {
        if let Some(num) = numbers.iter().find(|&j| i + j == target) {
            return Some((*i, *num));
        }
    }
    None
}

#[test]
fn test_sum_two() {
    let numbers = [1721, 979, 366, 299, 675, 1456];
    assert_eq!(part1(&numbers), 514579);
    assert_eq!(part1_hashmap(&numbers), 514579);
}

#[test]
fn test_sum_three() {
    let numbers = [1721, 979, 366, 299, 675, 1456];
    assert_eq!(part2(&numbers), 241861950);
}
